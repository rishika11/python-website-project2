﻿from django.contrib import admin
from django.db import models
from new1.models import *


class Enquiry_admin(admin.ModelAdmin):
	list_display=['name','email','phone','is_active']
	search_fields=['name','email']

class feedback_admin(admin.ModelAdmin):
	list_display=['course','idea']


admin.site.register(Enquiry,Enquiry_admin)
admin.site.register(feedback,feedback_admin)
admin.site.register(user_reg)
admin.site.register(project)
admin.site.register(proj_mca)
admin.site.register(graduate)