from django.db import models
from django.contrib.auth.models import User

	
class Enquiry(models.Model):
	name=models.CharField(max_length=100, null=False, blank=False)
	phone=models.BigIntegerField()
	email=models.EmailField(null=False,blank=False)
	query=models.CharField(max_length=200, null=False, blank=False)
	timestamp=models.DateTimeField(auto_now=True)
	is_active=models.BooleanField(default=True)
	
	def __unicode__(self):
		return self.email + str(self.phone)

class feedback(models.Model):
	course=models.CharField(max_length=25, null=False, blank=False)
	idea=models.BooleanField()
	assit=models.CharField(max_length=40)
	fileupload=models.FileField(upload_to="blueprint/")

class user_reg(models.Model):
	user=models.ForeignKey(User)
	college_name=models.CharField(max_length=30, null=False, blank=False)
	course_name=models.CharField(max_length=20, null=False, blank=False)
	_sem_yr=models.IntegerField(null=False, blank=False)
	_country=models.CharField(max_length=15, null=False, blank=False)
	


class project(models.Model):
		#proj=models.ForeignKey(project)
	proj_category=models.CharField(max_length=50, null=False, blank=False)
		

	
class graduate(models.Model):
	proj=models.ForeignKey(project)
	proj_category_grad=models.CharField(max_length=50, null=False, blank=False)


class proj_mca(models.Model):
	proje=models.ForeignKey(graduate)
	proj_title_mca=models.CharField(max_length=50, null=False, blank=False)
	proj_desc_mca=models.CharField(max_length=50, null=False, blank=False)

	

"""
class U_graduate(models.Model):
	p=models.ForeignKey(project)
	proj_category_Ugrad=models.CharField(max_length=50, null=False, blank=False)


class proj_bca(models.Model):
	pro=models.ForeignKey(graduate)
	proj_title_bca=models.CharField(max_length=50, null=False, blank=False)
	proj_desc_bca=models.CharField(max_length=50, null=False, blank=False)
"""